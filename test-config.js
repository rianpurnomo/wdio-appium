const path = require('path')
exports.config= {
    runner: 'local', 
    port: 4723, 
    host: '127.0.0.1',
    path: '/wd/hub', 
    logLevel: 'info', 
    framework: 'mocha',
    mochaOpts: {
        ui: 'bdd',
        required: ['@babel/register'],
        timeout: 600000
    },
    maxInstances: 10,
    sync: true,
    specs: [
        './test/**/*.js'
    ],
    // capabilities:[{
    //     "platformName": "Android",
    //     "automationName": "UiAutomator2",
    //     "udid": "2d211046",
    //     "appPackage": "com.example.movie_app",
    //     "appActivity": "com.example.movie_app.MainActivity"
    //   }],
    capabilities: [{
        "platformName": "Android",
        'appium:platformVersion': '10',
        "appium:deviceName": "MI 8 Lite",
        "appium:automationName": "UIAutomator2",
        "appium:app": path.join(process.cwd(), "./app/app-release.apk")
    }]
}